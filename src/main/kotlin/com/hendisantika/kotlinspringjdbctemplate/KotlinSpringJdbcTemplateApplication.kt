package com.hendisantika.kotlinspringjdbctemplate

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinSpringJdbcTemplateApplication

fun main(args: Array<String>) {
    runApplication<KotlinSpringJdbcTemplateApplication>(*args)
}
