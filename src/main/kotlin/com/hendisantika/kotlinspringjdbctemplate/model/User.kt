package com.hendisantika.kotlinspringjdbctemplate.model

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-spring-jdbc-template
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-26
 * Time: 08:28
 */
//Define a data class that maps to both our
//form and database table
data class User(var firstName: String = "",
                var lastName: String = "",
                var email: String = "",
                var phone: String = "")