package com.hendisantika.kotlinspringjdbctemplate.service

import com.hendisantika.kotlinspringjdbctemplate.model.User
import com.hendisantika.kotlinspringjdbctemplate.repository.IndexRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-spring-jdbc-template
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-26
 * Time: 08:30
 */
@Service
class IndexService(@Autowired var indexRepository: IndexRepository) {

    fun addUser(user: User) {
        indexRepository.addUser(user)
    }

    fun allUsers(): List<User> {
        return indexRepository.allUsers()
    }
}