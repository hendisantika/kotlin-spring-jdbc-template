# Kotlin Spring JDBC Template

### Run locally

```
git clone https://gitlab.com/hendisantika/kotlin-spring-jdbc-template.git
```

```
mvn clean spring-boot:run
```

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Add New Data

![Add New Data](img/add.png "Add New Data")